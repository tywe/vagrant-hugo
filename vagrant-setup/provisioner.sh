#!/bin/bash

################################################################################
# This script should be run as the 'vagrant' user, not root
# Use `privileged: false` in the shell provisioning line to do so
################################################################################

SSH_KEY=$1
SSH_CONFIG=$2
HUGO_PACKAGE=$3

chmod 600 $SSH_KEY
chmod 600 $SSH_CONFIG

# Set host IdentityFile for gitlab
# This will need to be fixed if other IdentityFile params don't use the same ssh key
sed -i "s|IdentityFile.*|IdentityFile $SSH_KEY|g" $SSH_CONFIG

# We're not using puppet
sudo rm -f /etc/yum.repos.d/puppetlabs-pc1.repo
sudo yum remove puppet -y

# Ehh this takes a while
# sudo yum update -y

if [ ! -f /tmp/hugo.rpm ]; then
  wget $HUGO_PACKAGE -O /tmp/hugo.rpm
  sudo rpm -iv /tmp/hugo.rpm
fi

# Open Hugo ports
sudo firewall-cmd --add-port 1313/tcp --permanent
sudo firewall-cmd --reload

sudo chown -R vagrant:vagrant /home/vagrant

# To use the live Hugo server on the vagrant machine, use the following:
# hugo server --bind 10.0.2.15     (or whatever IP is not 127.0.0.1 on the VM)
