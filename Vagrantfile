# -*- mode: ruby -*-
# vi: set ft=ruby :

# Config file setup
# Create/modify variables in ./vagrant-setup/config.yaml
# Change the 'profile' variable to switch as needed
# Thanks http://stackoverflow.com/a/26394449/4496695
require 'yaml'

current_dir    = File.dirname(File.expand_path(__FILE__))
config_file    = YAML.load_file("#{current_dir}/vagrant-setup/config.yaml")
vagrant_config = config_file['config']
os_config      = vagrant_config['os_configs'][vagrant_config['os']]

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "puppetlabs/centos-7.2-64-puppet"


  ################################
  # Synced folders and files
  ################################

  # Main
  config.vm.synced_folder "./sync", "/home/vagrant/sync"

  # Hugo
  config.vm.synced_folder "#{os_config['host_hugo']}",
    "#{vagrant_config['local_hugo']}",
    :mount_options => ["rw"]

  # Set up git auth
  config.vm.provision "file",
    source: "#{os_config['ssh_key_location']}/#{vagrant_config['ssh_key_name']}",
    destination: "~/.ssh/#{vagrant_config['ssh_key_name']}"
  config.vm.provision "file",
    source: "./vagrant-setup/.ssh/config",
    destination: "~/.ssh/config"


  ################################
  # Provisioning scripts
  ################################

  # Run initial provisioning script as vagrant user
  config.vm.provision "shell", path: "./vagrant-setup/provisioner.sh", privileged: false do |s|
    s.args = [
      "/home/vagrant/.ssh/#{vagrant_config['ssh_key_name']}",
      "/home/vagrant/.ssh/config",
      "#{vagrant_config['hugo_package']}"
    ]
  end


  ################################
  # VM provider config
  ################################

  config.vm.provider "virtualbox" do |vb|
    # Set VM name
    vb.name = "#{vagrant_config['box_name']}"
  end

  config.vm.network :forwarded_port, guest: 1313, host: 1313

end
